import java.util.Arrays;

import org.jacop.constraints.*;
import org.jacop.constraints.netflow.*;
import org.jacop.constraints.netflow.simplex.Node;
import org.jacop.core.*;
import org.jacop.search.*;

public class examplemain {

	public static void main(String[] args) {		
		examplemain.exempel(1);
		}
	
	public static void run(int capability,int power, int nbr_dests, int nbr_source, int nbr_nodes, int nbr_internalArcs, int[] from,
			int[] to,int[] cost) {
		 // Power is describing the amount that will flow throw the network (nbr of packeges, L of water)
		 // Capability is the amount that a node can handle 
		
		int totnbr_arcs = nbr_dests + nbr_source + nbr_internalArcs*2; // *2 to make a->b and a<-b 
		
		Node[] nodes = new Node[nbr_nodes];
		
		//Init store
		Store store = new Store();
		
		//Init network, source and sink
		NetworkBuilder nb = new NetworkBuilder();

		//Init all arcs
		IntVar[] arcs = new IntVar[totnbr_arcs];
		
		int[] costvec = new int[totnbr_arcs];
		IntVar[] arcIfUsed = new IntVar[totnbr_arcs];
		IntVar totcost = new IntVar(store,"Total_cost",0,1000);
		
		for(int i=0;i<arcIfUsed.length;i++) {
			arcIfUsed[i] = new IntVar(store,0,1);
		}
		
		//Init all nodes and add them to the network (manually) (try to make loop!)
			//// Sink and source
			Node sink = nb.addNode("sink", -power); 
			Node source = nb.addNode("source", power); 
			
			//// Internal nodes
			for(int i =0;i<nbr_nodes;i++) {
				nodes[i] = nb.addNode(" "+i+1,0);
			}
		// Converting all nodes to IntVars and puting them in arcs AND! adding arcs to network
			//// Source
			for(int i=0;i<nbr_source;i++) {
				arcs[i] = new IntVar(store,"Source_"+i, 0,capability);
				nb.addArc(source,nodes[i],0,arcs[i]);
			}
			//// Sink
			for(int i = 0; i<nbr_dests;i++) {
				arcs[totnbr_arcs-i-1] = new IntVar(store,"Destination_"+i,0,capability);
				nb.addArc(nodes[nbr_nodes-i-1], sink, 0, arcs[totnbr_arcs-i-1]); // Might not be the best way becuse now we allwas take the last nodes as destinations
			}
			//// Internal nodes (you are able to move in bouth directiones on all conectiones (becuse of that nbr_internalArcs*2)
			for(int i = nbr_source;i<nbr_internalArcs*2+nbr_source;i++) {
				if(i<nbr_internalArcs+nbr_source) {
					arcs[i] = new IntVar(store,"from_"+(from[i-nbr_source])+"_to_"+(to[i-nbr_source]),0,capability);
					nb.addArc(nodes[from[i-nbr_source]-1], nodes[to[i-nbr_source]-1],cost[i-1],arcs[i]);
					costvec[i] = cost[i-nbr_source];
				} else {
					arcs[i] = new IntVar(store,"from_"+(to[i-nbr_source-nbr_internalArcs])+"_to_"+(from[i-nbr_source-nbr_internalArcs]),0,capability); //Subtracting with nbr_internalArcs to be able to have bouth way initzialacation
					nb.addArc(nodes[to[i-nbr_internalArcs-nbr_source]-1], nodes[from[i-nbr_internalArcs-nbr_source]-1],cost[i-nbr_internalArcs-nbr_source],arcs[i]);
					costvec[i] = cost[i-nbr_internalArcs-nbr_source];

				}
			}
		for(int i=0;i<arcs.length;i++) {
			store.impose(new Reified(new XgteqC(arcs[i],1),arcIfUsed[i]));
		}
		store.impose(new LinearInt(arcIfUsed, costvec, "==", totcost));
		
		nb.setCostVariable(new IntVar(store,"cost",0,1000));
			
		store.impose(new NetworkFlow(nb));
		
		// All the goals need to be visited
		System.out.println();
		for(int i = 0;i<nbr_dests;i++) {
			store.impose(new XgtC(arcs[totnbr_arcs-i-1],0));
		}
		
		Search<IntVar> search = new DepthFirstSearch<IntVar>();
		SelectChoicePoint<IntVar> select = new InputOrderSelect<IntVar>(store,
				arcs, new IndomainMin<IntVar>());
		boolean result = search.labeling(store, select, totcost);
		if (result) {
			System.out.println("Cost: " + search.getCostValue());
			
	}
	
		

}
	public static void exempel(int i) {
		if(i==1) {
			int graph_size = 6;
			int start = 1;
			int n_dests = 1;
			int[] dest = {6};
			int n_edges = 7;
			int[] from = {1,1,2,2,3,4,4};
			int[] to =   {2,3,3,4,5,5,6};
			int[] cost = {4,2,5,10,3,4,11};
			run(n_dests,n_dests, n_dests, 1, graph_size, n_edges, from, to, cost);
				
		}else if(i==2) {
			int graph_size = 6;
			int start = 1;
			int n_dests = 2;
			int[] dest = {5,6};
			int n_edges = 7;
			int[] from = {1,1,2, 2,3,4, 4};
			int[] to = {2,3,3, 4,5,5, 6};
			int[] cost = {4,2,5,10,3,4,11};
			run(n_dests,n_dests, n_dests, 1, graph_size, n_edges, from, to, cost);

		}else if(i==3) {
			int graph_size = 6;
			int start = 1;
			int n_dests = 2;
			int[] dest = {5,6};
			int n_edges = 9;
			int[] from = {1,1,1,2,2,3,3,3,4};
			int[] to = 	 {2,3,4,3,5,4,5,6,6};
			int[] cost = {6,1,5,5,3,5,6,4,2};
			run(n_dests,n_dests, n_dests, 1, graph_size, n_edges, from, to, cost);

		}
	}
}
