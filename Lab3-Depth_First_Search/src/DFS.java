import org.jacop.core.IntVar;

public interface DFS {
    public void setVariablesToReport(IntVar[] v);
    public void setCostVariable(IntVar v);
    public boolean label(IntVar[] vars);


}
