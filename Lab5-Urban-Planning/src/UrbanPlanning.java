import org.jacop.*;
import org.jacop.constraints.*;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.search.*;
 
public class UrbanPlanning {
 
    public static void main(String[] args) {
        // DATASET 1
//      int n = 5;
//      int n_commercial = 13;
//      int n_residential = 12;
//      int[] point_distribution = { -5, -4, -3, 3, 4, 5 };
 
        // DATASET 2
      int n = 5;
      int n_commercial = 7;
      int n_residential = 18;
      int[] point_distribution =
      {-5, -4, -3, 3, 4, 5};
 
        // DATASET 3
//        int n = 7;
//        int n_commercial = 20;
//        int n_residential = 29;
//        int[] point_distribution = { -7, -6, -5, -4, 4, 5, 6, 7 };
 
        Store store = new Store();
 
        IntVar cost = new IntVar(store, 1, 59);
 
        IntVar[][] grid = new IntVar[n][n];
        IntVar[] cols = new IntVar[n];
        IntVar[] rows = new IntVar[n];
 
        IntVar[] allRowsHouses = new IntVar[n];
        IntVar[] allColsHouses = new IntVar[n];
 
        IntVar[] rowHouseIndex = new IntVar[n];
        IntVar[] colHouseIndex = new IntVar[n];
 
        IntVar[] rowsScore = new IntVar[n];
        IntVar[] colsScore = new IntVar[n];
 
        IntVar totRowScore = new IntVar(store, "sumRows:", -n * n, n * n);
        IntVar totColScore = new IntVar(store, "sumRows:", -n * n, n * n);
 
        IntVar totNbrHouses = new IntVar(store, "total nbr houses: ", 0, n * n);
 
        IntVar negConstant = new IntVar(store, -1, -1);
        IntVar negCost = new IntVar(store, "negative cost to minimize", -n * n, n * n);
 
        // 0 represents store -- 1 represents house
        // Init all IntVars incuding grid
        for (int r = 0; r < n; r++) {
            for (int c = 0; c < n; c++) {
                grid[r][c] = new IntVar(store, "row:" + (r + 1) + ", column:" + (c + 1), 0, 1);
            }
 
            // Init all IntVar with n elements
            allRowsHouses[r] = new IntVar(store, "sum rows:", -n, n);
            allColsHouses[r] = new IntVar(store, "sum cols:", -n, n);
            rows[r] = new IntVar(store, "", -n, n);
            cols[r] = new IntVar(store, "", -n, n);
 
            rowsScore[r] = new IntVar(store, " rows score:", -n, n);
            colsScore[r] = new IntVar(store, "cols scores:", -n, n);
 
            rowHouseIndex[r] = new IntVar(store, " from point_distrubution get value :", 0, n);
        }
 
        // Geting rows as Vectors
        for (int r = 0; r < n; r++) {
            store.impose(new SumInt(grid[r], "==", allRowsHouses[r]));
        }
 
        // Geting cols as Vectors
        for (int c = 0; c < n; c++) {
            store.impose(new SumInt(getCol(grid, c, n), "==", allColsHouses[c]));
        }
 
        store.impose(new SumInt(allRowsHouses, "==", totNbrHouses));
        store.impose(new XeqC(totNbrHouses, n_residential));
 
        // Number of houses represents the index to fetch from point_distrubution
        rowHouseIndex = allRowsHouses;
        colHouseIndex = allColsHouses;
 
        for (int i = 0; i < n; i++) {
            store.impose(new ElementInteger(rowHouseIndex[i], point_distribution, rowsScore[i], -1));
            store.impose(new ElementInteger(colHouseIndex[i], point_distribution, colsScore[i], -1));
        }
 
        store.impose(new SumInt(rowsScore, "==", totRowScore));
        store.impose(new SumInt(colsScore, "==", totColScore));
 
        store.impose(new XplusYeqZ(totRowScore, totColScore, cost));
 
        store.impose(new XmulYeqZ(negConstant, cost, negCost));
 
//        for (int i = 1; i < n - 1; i++) {
//            store.impose(new LexOrder(getCol(grid, i, n), getCol(grid, i+1, n)));
//            store.impose(new LexOrder(grid[i], grid[i + 1]));
//        }
 
        for (int i = 0; i < n - 1; i++) {
            IntVar Row0 = new IntVar(store, 0, n);
            IntVar Row1 = new IntVar(store, 0, n);
            store.impose(new SumInt(grid[i], "==", Row0));
            store.impose(new SumInt(grid[i + 1], "==", Row1));
            store.impose(new XgteqY(Row0, Row1));
 
            IntVar Col0 = new IntVar(store, 0, n);
            IntVar Col1 = new IntVar(store, 0, n);
            store.impose(new SumInt(getCol(grid, i, n), "==", Col0));
            store.impose(new SumInt(getCol(grid, i + 1, n), "==", Col1));
            store.impose(new XgteqY(Col0, Col1));
        }
 
        // Pron and find solutions
        Search<IntVar> search = new DepthFirstSearch<IntVar>();
        SelectChoicePoint<IntVar> select = new SimpleMatrixSelect<IntVar>(grid, null, new IndomainMin<IntVar>());
 
        boolean result = search.labeling(store, select, negCost);
        if (result) {
            System.out.println("Total score: " + cost);
            StringBuilder sb = new StringBuilder();
 
            for (int r = 0; r < n; r++) {
                for (int c = 0; c < n; c++) {
                    sb.append(grid[r][c] + " ");
                }
                sb.append("\n");
            }
 
            System.out.println(sb);
        } else {
            System.out.println("No solution found.");
        }
 
    }
 
    public static IntVar[] getCol(IntVar[][] grid, int i, int n) {
        IntVar[] col = new IntVar[n];
 
        int prevIndex = 0;
        for (int r = 0; r < n; r++) {
            col[r] = grid[r][i];
        }
        return col;
    }
 
}
