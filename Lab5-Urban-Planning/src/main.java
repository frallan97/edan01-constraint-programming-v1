import org.jacop.constraints.*;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.search.*;

public class main {

	public static void main(String[] args) {
		DataSet1 data = new DataSet1();
		Store store = new Store();
		
		IntVar cost = new IntVar(store,"Total cost: ",1,1000);
		
		IntVar[][] grid = new IntVar[data.n][data.n];
		
		IntVar[] allRow = new IntVar[data.n];
		IntVar[] allCol = new IntVar[data.n];
		
		IntVar sumRows = new IntVar(store,"sumRows:",-data.n_commercial,data.n_residential);
		IntVar sumCols = new IntVar(store,"sumRows:",-data.n_commercial,data.n_residential);

		
		// 0 reprecents house -- 1 reprecents store
		for(int i = 0; i<data.n;i++) {
			for(int j = 0; j<data.n;i++) {
				grid[i][j] = new IntVar(store, "row:"+(i+1)+", column:"+(j+1),0,1); 
			}
			allRow[i] = new IntVar(store, "sum row:",0,data.n);
			allCol[i] = new IntVar(store, "sum col:",0,data.n);
		}
		
		store.impose(new XplusYeqZ(sumCols, sumRows, cost));
		
        
		
		// Pron and find solutions
		Search<IntVar> search = new DepthFirstSearch<IntVar>();
        SelectChoicePoint<IntVar> select = new SimpleMatrixSelect<IntVar>(grid, null, new IndomainMin<IntVar>());
        
        boolean result = search.labeling(store, select, cost);
        if (result) {
            System.out.println("Solution : " + java.util.Arrays.asList(cost));
            for(int i = 0; i<data.n;i++) {
    			for(int j = 0; j<data.n;i++) {
    			
    			}
            }
        } else {
            System.out.println("No solution found.");
        }
		
		
		
	}

}
