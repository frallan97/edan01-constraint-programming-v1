/**
 *  SimpleDFS.java 
 *  This file is part of JaCoP.
 *
 *  JaCoP is a Java Constraint Programming solver. 
 *	
 *	Copyright (C) 2000-2008 Krzysztof Kuchcinski and Radoslaw Szymanek
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  Notwithstanding any other provision of this License, the copyright
 *  owners of this work supplement the terms of this License with terms
 *  prohibiting misrepresentation of the origin of this work and requiring
 *  that modified versions of this work be marked in reasonable ways as
 *  different from the original version. This supplement of the license
 *  terms is in accordance with Section 7 of GNU Affero General Public
 *  License version 3.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.awt.Choice;

import org.jacop.constraints.Not;
import org.jacop.constraints.PrimitiveConstraint;
import org.jacop.constraints.XeqC;
import org.jacop.constraints.XgteqC;
import org.jacop.constraints.XlteqC;
import org.jacop.core.FailException;
import org.jacop.core.IntDomain;
import org.jacop.core.IntVar;
import org.jacop.core.Store;

/**
 * Implements Simple Depth First Search .
 * 
 * @author Krzysztof Kuchcinski
 * @version 4.1
 */

public class CustomDFS implements DFS {
	
	

    //Values to determine seartch method
	int varSelectMethod;
	int valSelectMethod;
	
	boolean trace = false;

    //Store used in search
    Store store;

    // Defines varibales to be printed when solution is found
    IntVar[] variablesToReport;

    
    // It represents current depth of store used in search.
    int depth = 0;

    // It represents the cost value of currently best solution for FloatVar cost.
    public int costValue = IntDomain.MaxInt;

    // It represents the cost variable.
    public IntVar costVariable = null;
    
    // Value reprecents amount of wrong choises
    int nbrWrongChoice = 0;
    
    // Value reprecenting nbr of nodes
    int nbrNodes = 0;
    
	ChoicePoint choice = null;


    public CustomDFS(Store s,int varSelectMethod,int valSelectMethod) {
    	store = s;
    	
    	//Values to select to determine seartch method
    	this.varSelectMethod = varSelectMethod;
    	this.valSelectMethod = valSelectMethod;
    	
    }


    // This function is called recursively to assign variables one by one.
    public boolean label(IntVar[] vars) {

	if (trace) {
	    for (int i = 0; i < vars.length; i++) 
		System.out.print (vars[i] + " ");
	    System.out.println ();
	}

	boolean consistent;

	// Instead of imposing constraint just restrict bounds
	// -1 since costValue is the cost of last solution
	if (costVariable != null) {
	    try {
		if (costVariable.min() <= costValue - 1)
		    costVariable.domain.in(store.level, costVariable, costVariable.min(), costValue - 1);
		else
		    return false;
	    } catch (FailException f) {
		return false;
	    }
	}

	consistent = store.consistency();
		
	if (!consistent) {
	    // Failed leaf of the search tree
		nbrWrongChoice++;
	    return false;
	} else { // consistent

	    if (vars.length == 0) {
		// solution found; no more variables to label

		// update cost if minimization
		if (costVariable != null)
		    costValue = costVariable.min();

		reportSolution();

		return costVariable == null; // true is satisfiability search and false if minimization
	    }

 	    choice = new ChoicePoint(vars);

	    levelUp();

	    store.impose(choice.getConstraint());

	    // choice point imposed.
			
	    consistent = label(choice.getSearchVariables());

        if (consistent) {
        	levelDown();
        	return true;
	    } else {

		restoreLevel();

		store.impose(new Not(choice.getConstraint()));

		// negated choice point imposed.
		
		consistent = label(vars);

		levelDown();

		if (consistent) {
		    return true;
		} else {
		    return false;
		}
	    }
	}
    }

    void levelDown() {
	store.removeLevel(depth);
	store.setLevel(--depth);
    }

    void levelUp() {
	store.setLevel(++depth);
    }

    void restoreLevel() {
	store.removeLevel(depth);
	store.setLevel(store.level);
    }

    public void reportSolution() {
	if (costVariable != null)
	    System.out.println ("Cost is " + costVariable);

	for (int i = 0; i < variablesToReport.length; i++) 
	    System.out.print (variablesToReport[i] + " ");
	System.out.println ("\n---------------");
	
	System.out.println("Variable selection method: "+choice.getVarMethod());
	System.out.println("Value selection method: "+choice.getValMethod());
	System.out.println("Nbr bad choices: " +nbrWrongChoice);
	System.out.println("nbr of nodes visited: "+nbrNodes);
    }

    public void setVariablesToReport(IntVar[] v) {
	variablesToReport = v;
    }

    public void setCostVariable(IntVar v) {
	costVariable = v;
    }

    
    // Where to seartch and customice your seartch
    public class ChoicePoint {    	
	IntVar var;
	IntVar[] searchVariables;
	int value;
	String varMethod;
	String valMethod;

	public ChoicePoint (IntVar[] v) {
	    var = selectVariable(v);
	    value = selectValue(var);
	}

	public IntVar[] getSearchVariables() {
	    return searchVariables;
	}

	//example variable selection; input order
	IntVar selectVariable(IntVar[] v) {
	    int index = 0;
	    nbrNodes++;
		
		if (v.length != 0) {
	    	searchVariables = new IntVar[v.length-1];

	    	// Init seartchVariables to be used in different cases
	    	switch (varSelectMethod) {
	    	
	    	// In-Order (remove index [0] until v.lenght == 0)
	    	case 1:
	    		varMethod = "In-Order";
				for (int i = 0; i < v.length-1; i++) {
				    searchVariables[i] = v[i+1]; 
				}
				return v[0];
			
			// Most Constraints	(remove index with most constraints until v.lenght == 0)
	    	case 2:
	    		varMethod = "Most-Constraints";

				for (int i = 0; i < v.length; i++) {
				    if(v[i].sizeConstraints() > v[index].sizeConstraints()) {
				    	index = i;
				    } 
				}
				// when we found index with most constraints we remove it
				for(int i = 0; i < v.length-1; i++) {
					if(i<index) searchVariables[i] = v[i];
					else searchVariables[i] = v[i+1];
				}
	    		return v[index];
	    		
	    	// Chosing the var always in the middle 	
	    	case 3:
	    		varMethod = "Chosing-Midle";
	    		for (int i = 0; i < v.length-1; i++) {
				    if(i<v.length/2) searchVariables[i] = v[i];
				    else searchVariables[i] = v[i+1];
				}
	    		return v[v.length/2]; 
	 
	    	// Reverse In-Order
	    	case 4:
	    		for (int i = 0; i < v.length-1; i++) {
				     searchVariables[i] = v[v.length-i-2]; // Kanske inte borde anv�nda "-2" kan ge indexproblem
				}
	    		return v[v.length-1]; 
	    	
	    	// First fail 
	    	case 5:
				for (int i = 0; i < v.length; i++) {
				    if(v[i].sizeConstraints() > v[index].sizeConstraints()) {
				    	index = i;
				    } 
				}
				// when we found index with most constraints we remove it
				for(int i = 0; i < v.length-1; i++) {
					if(i<index) searchVariables[i] = v[i];
					else searchVariables[i] = v[i+1];
				}
	    		return v[index];
	    	}
	    	
	    } else {
		System.err.println("Zero length list of variables for labeling");
		return new IntVar(store);
	    }
	    return v[0];   //Must have defult value 
	}
	
	// example value selection; indomain_min
	int selectValue(IntVar v) {
		switch (valSelectMethod) {
		case(1):
			valMethod = "v.min() & XeqC(var, value)";
			return v.min();
		case(2):
			valMethod = "v.max() & XlteqC(var, value)";
			return v.max();
		case(3):
			valMethod = "(v.min()+v.max())/2 & XgteqC(var,value)";
			if((v.min()+v.max()) % 2 == 0){
				return (v.min()+v.max())/2;
			}
			return ((v.min()+v.max())/2)+1; // Automatic round up 
		}
		return v.sizeConstraints();
	}

	// example constraint assigning a selected value
	public PrimitiveConstraint getConstraint() {
		switch (valSelectMethod) {
			case 1:
				return new XeqC(var, value);
			case 2:
				return new XlteqC(var, value);
			case 3:
				return new XgteqC(var,value);
		}
	    return new XlteqC(var, value);
	}
	
	public String getVarMethod() {
		return varMethod;
	}
	public String getValMethod() {
		return valMethod;
	}
	
    }
}

