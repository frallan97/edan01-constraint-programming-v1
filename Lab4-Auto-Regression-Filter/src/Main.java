import java.util.Arrays;

import org.jacop.constraints.Cumulative;
import org.jacop.constraints.Max;
import org.jacop.constraints.XplusClteqZ;
import org.jacop.constraints.XplusYeqZ;
import org.jacop.constraints.XplusYlteqZ;
import org.jacop.constraints.diffn.Diffn;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.IndomainMin;
import org.jacop.search.MostConstrainedStatic;
import org.jacop.search.Search;
import org.jacop.search.SelectChoicePoint;
import org.jacop.search.SimpleSelect;

public class Main {
	
	public static void main(String[] args) {

		DataSet2 data = new DataSet2();
		Store store = new Store();
		
		System.out.println("Number of adds: " + data.add.length);
		System.out.println("Number of muls: " + data.mul.length);
		
		
		// Init Arrays
		IntVar[] addStart = new IntVar[data.add.length];
		IntVar[] mulStart = new IntVar[data.mul.length];
		IntVar[] allStart = new IntVar[data.add.length + data.mul.length];
		
		IntVar[] allEnd	  = new IntVar[data.add.length + data.mul.length];
		
		IntVar[] addDelay = new IntVar[data.add.length];
		IntVar[] mulDelay = new IntVar[data.mul.length];
		IntVar[] allDelay = new IntVar[data.add.length + data.mul.length];
		
		IntVar[] addProcUsed = new IntVar[data.add.length];
		IntVar[] mulProcUsed = new IntVar[data.mul.length];
		IntVar[] allProcUsed = new IntVar[data.add.length + data.mul.length];
		
		IntVar[][] addRects = new IntVar[data.add.length][4];	//first index for rectangle, second index: 0- start x, 1- start y, 2- length x, 3- length y
		IntVar[][] mulRects = new IntVar[data.mul.length][4];
		
		IntVar[] addProcNeed = new IntVar[data.add.length];
		IntVar[] mulProcNeed = new IntVar[data.mul.length];

		
		// Init IntVars		
		IntVar addLimit = new IntVar(store, "Limit", data.number_add, data.number_add);
		IntVar mulLimit = new IntVar(store, "Limit", data.del_mul, data.del_mul);
		
		IntVar ProcNeeded = new IntVar(store,"Processes needed for operation",1,1);
		
		IntVar cost = new IntVar(store, "Cost", 0, 100);
		
		
		for(int i = 0; i < data.n; i++) {
			allStart[i] = new IntVar(store, "Task " + (i+1) + " start", 0, 50);
			allEnd[i] = new IntVar(store, "Task " + (i+1) + " end", 0, 50);
		}
		
		// Fill Arrays with IntVars
		//Add
		for (int i=0;i<data.add.length;i++) {
			addStart[i] = allStart[data.add[i]-1];
			addProcUsed[i] = new IntVar(store, "Adding task " + (data.add[i]) + " used processor ", 1, data.number_add);
			addProcNeed[i] = ProcNeeded;
			addDelay[i] = new IntVar(store, data.del_add, data.del_add);
			
			
			allDelay[data.add[i] - 1] = addDelay[i];
			allProcUsed[data.add[i] - 1] = addProcUsed[i];
		}
		
		// Mul
		for (int i=0;i<data.mul.length;i++) {
			mulStart[i] = allStart[data.mul[i]-1];
			mulProcUsed[i] = new IntVar(store, "Mul task " + (data.mul[i]) + " used processor ", 1, data.number_mul);
			mulProcNeed[i] = ProcNeeded;
			mulDelay[i] = new IntVar(store, data.del_mul, data.del_mul);
			
			
			allDelay[data.mul[i] - 1] = mulDelay[i];
			allProcUsed[data.mul[i] - 1] = mulProcUsed[i];
		}		
		
		// Start Imposing Constraints
		// Imposing that start time plus delay should be end time
		for(int i = 0; i < allEnd.length; i++) {
			store.impose(new XplusYeqZ(allStart[i], allDelay[i], allEnd[i]));
		}
		
		// Imposing all dependencies.
		for(int i = 0; i < data.n; i++) {
			for(int j = 0; j < data.dependencies[i].length; j++) {
				//Check if the active operation is an add or mul
				IntVar tempDelay = allDelay[data.dependencies[i][j] - 1];
				
				store.impose(new XplusYlteqZ(allStart[i], allDelay[i], allStart[data.dependencies[i][j]-1]));
				System.out.println(data.dependencies[i][j] + " has to be executed after task " + allStart[i]);
			}
		}

		
//		// Imposing end nodes
//		for(int i = 0; i < allStart.length; i++) {
//			for(int j = 0; j < data.last.length; j++) {
//				
//
//				if(allStart[i] != allStart[data.last[j] - 1]) {
//					//store.impose(new XgteqY(allStart[data.last[j] - 1], allStart[i]));
//					store.impose(new XplusYlteqZ(allDelay[i], allStart[i], allStart[data.last[j] - 1]));
//					System.out.println("Execution start time for " + allStart[data.last[j] - 1] + " is after " + allStart[i]);
//				} else {
//					System.out.println(allStart[i] + " is one of the last operations.");
//				}
//			}
//		}
		
		boolean isLast = false;
		for(int i = 0; i < allStart.length; i++) {
			for(int j = 0; j < data.last.length; j++) {
				if(allStart[i] == allStart[data.last[j] - 1]) {
					isLast = true;
					break;
				}
			}
			
			for(int j = 0; j < data.last.length; j++) {
				if(!isLast) {
					//store.impose(new XgteqY(allStart[data.last[j] - 1], allStart[i]));
					store.impose(new XplusYlteqZ(allDelay[i], allStart[i], allStart[data.last[j] - 1]));
					System.out.println("Execution start time for " + allStart[data.last[j] - 1] + " is after " + allStart[i]);
				} else {
					System.out.println(allStart[i] + " is one of the last operations.");
				}
			}
			isLast = false;
		}
		
		//Impose cumulative constr for add
		store.impose(new Cumulative(addStart, addDelay, addProcNeed, addLimit));
				
		//Impose cumulative constr for mul
		store.impose(new Cumulative(mulStart, mulDelay, mulProcNeed, mulLimit));
		
		//Implementera diff ctr
		
		//Create diff rectangles for adding
		for(int i = 0; i < data.add.length; i++) {
			addRects[i][0] = addStart[i];
			addRects[i][1] = addProcUsed[i];
			addRects[i][2] = addDelay[i];
			addRects[i][3] = new IntVar(store, 1, 1);	//Each operation only needs one processor. Height of rect is always 1.
		}
		
		//Create diff rectangles for mulling
		for(int i = 0; i < data.mul.length; i++) {
			mulRects[i][0] = mulStart[i];
			mulRects[i][1] = mulProcUsed[i];
			mulRects[i][2] = mulDelay[i];
			mulRects[i][3] = new IntVar(store, 1, 1);	//Each operation only needs one processor. Height of rect is always 1.
		}
			
		store.impose(new Diffn(addRects));
		store.impose(new Diffn(mulRects));
			
		store.impose(new Max(allEnd, cost));
				
		//Solve and search
		Search<IntVar> slave = new DepthFirstSearch<IntVar>();
		SelectChoicePoint slaveSelect = new SimpleSelect<IntVar>(allProcUsed, new MostConstrainedStatic<IntVar>(), new IndomainMin<IntVar>());
		slave.setSelectChoicePoint(slaveSelect);
		
		Search<IntVar> search = new DepthFirstSearch<IntVar>();
		search.addChildSearch(slave);
		search.setPrintInfo(true);
		SelectChoicePoint<IntVar> select = new SimpleSelect<IntVar>(allStart, new MostConstrainedStatic<IntVar>(), new IndomainMin<IntVar>());
				
		boolean result = search.labeling(store, select, cost);
			
		//Print result
		if(result) {
			for(int i = 0; i < allStart.length; i++) {
				System.out.println(allStart[i] + "-----" + allProcUsed[i]);
			}

		} else {
			System.out.println("No solution.");
		}
		
	}
}
